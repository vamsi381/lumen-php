<?php

namespace App\Http\Controllers;

use App\Models\Notes;
use Illuminate\Http\Response;


class NotesController extends Controller
{

    /**
     * The user repository instance.
     */
    protected $notes;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Notes $notes)
    {
        //
        $this->notes = $notes;
    }

    //

    public function index()
    {
        $result = Notes::all();

        $notes_info = [];
        foreach ($result as $key => $data) {
            $notes_info[$key]['title'] = $data['title'];
            $notes_info[$key]['description'] = $data['description'];
            $notes_info[$key]['created_at'] = $data['created_at'];
        }

        return (new Response($notes_info, 200));
    }
}
