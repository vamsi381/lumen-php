<?php

namespace App\Http\Controllers;

use App\Models\Tags;
use Illuminate\Http\Request;
use Illuminate\Http\Response;


class TagsController extends Controller
{

    /**
     * The user repository instance.
     */
    protected $tags;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Tags $tags)
    {
        //
        $this->tags = $tags;
    }

    //

    public function index()
    {
        $result = Tags::all();

        $tags_info = [];
        foreach ($result as $key => $data) {
            $tags_info[$key]['tag_name'] = $data['tag_name'];
        }

        return (new Response($tags_info, 200));
    }

    public function store(Request $request)
    {
        dd($request->input('tagName'));
    }
}

?>
