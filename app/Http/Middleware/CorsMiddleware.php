<?php

namespace App\Http\Middleware;

use Closure;

class CorsMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $headers = [
            'Access-Control-Allow-Origin' => '*',
            'Access-Control-Allow-Methods' => 'HEAD, GET, POST, PUT, PATCH, DELETE',
            'Access-Control-Allow-Headers' => '*',
            'Access-Control-Allow-Credentials' => 'true'

        ];

        //Intercepts OPTIONS requests
        if($request->isMethod('OPTIONS')) {
            $response = response('ok', 200,$headers);
        }

        $response = $next($request);


        if (\method_exists($response, 'header')) {
            // Adds headers to the response
            $response->headers->set('Access-Control-Allow-Methods', 'HEAD, GET, POST, PUT, PATCH, DELETE');
            $response->headers->set('Access-Control-Allow-Headers', 'Content-Type,Accept,Authorization,X-Requested-With,Origin,Application');
            $response->headers->set('Access-Control-Allow-Origin', '*');
            $response->headers->set('Access-Control-Allow-Credentials', 'true');
        }

        if($response instanceof \Illuminate\Http\Response)
        {
            foreach($headers as $key=>$value)
            {
                $response->header($key,$value);
            }

            return $response;
        }

        if($response instanceof \Symfony\Component\HttpFoundation\Response){
            foreach($headers as $key=>$value)
            {
                $response->headers->set($key,$value);
            }

            return $response;
        }


        // Sends it
        return $response;
    }
}
